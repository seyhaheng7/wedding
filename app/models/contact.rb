class Contact < ApplicationRecord
	def self.import(file)
    counter = 0
		CSV.foreach(file.path, headers: true, header_converters: :symbol) do |row|
			contact = Contact.assign_from_row(row)
      if contact.save
        counter += 1
      else
        puts "#{contact.name} - #{contact.errors.full_messages.jion(",")}"
      end
		end
    counter
	end

	extend FriendlyId

  friendly_id :name, use: [:slugged, :history, :finders]

  def should_generate_new_friendly_id?
    name_changed?
  end

	def self.assign_from_row(row)
    contact = Contact.where(name: row[:name]).first_or_initialize
    contact.assign_attributes row.to_hash
    contact
  end
	# def self.open_spreadsheet(file)
	# 	case File.extname(file.original_filename)
	# 		when ".csv" then Roo::CSV.new(file.path, nil, :ignore)
	# 		when ".xls" then Roo::Excel.new(file.path, nil, :ignore)
	# 		when ".xlsx" then Roo::Excelx.new(file.path, nil, :ignore)
	# 		else raise "Unknown file type: #{file.original_filename}"
	# 	end
	# end

end
