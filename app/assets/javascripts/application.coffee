#= require rails-ujs
#= require jquery
#= require jquery_ujs
#= require bootstrap
#= require jquery.countdown
#= require dscountdown

#= require namespace
#= require common
#= require util
#= require initializer
