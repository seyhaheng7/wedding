Invitation.Initializer =
  exec: (pageName) ->
    if pageName && Invitation[pageName]
      Invitation[pageName]['init']()

  currentPage: ->
    return '' unless $('body').attr('id')

    bodyIds     = $('body').attr('id').split('_')
    pageName    = ''
    for bodyId in bodyIds
      pageName += Invitation.Util.capitalize(bodyId)
    pageName

  init: ->
    Invitation.Initializer.exec('Common')
    if @currentPage()
      Invitation.Initializer.exec(@currentPage())

$(document).on 'ready page:load', ->
  Invitation.Initializer.init()
