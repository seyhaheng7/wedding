# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path.
# Rails.application.config.assets.paths << Emoji.images_path
# Add Yarn node_modules folder to the asset load path.
Rails.application.config.assets.paths << Rails.root.join('node_modules')

# Precompile additional assets.
# Rails.application.config.assets.precompile += %w( CSS in the app/asset)
# folder are already added.
# Rails.application.config.assets.precompile += %w( admin.js admin.css )
Rails.application.config.assets.precompile += %w( wp-content/plugins/layerslider/static/layerslider/js/greensock.js )
Rails.application.config.assets.precompile += %w( wp-content/plugins/layerslider/static/layerslider/js/layerslider.transitions.js )
Rails.application.config.assets.precompile += %w( wp-content/plugins/vamtam-love-it/includes/js/jquery.cookie.js )
Rails.application.config.assets.precompile += %w( wp-includes/js/jquery/jquery.js )
Rails.application.config.assets.precompile += %w( wp-includes/js/jquery/jquery-migrate.min.js )
Rails.application.config.assets.precompile += %w( wp-content/plugins/layerslider/static/layerslider/js/layerslider.kreaturamedia.jquery.js )
Rails.application.config.assets.precompile += %w( wp-content/plugins/contact-form-7/includes/js/scripts.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/underscore.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/backbone.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/plugins/vamtam-love-it/includes/js/love-it.js )
Rails.application.config.assets.precompile += %w( wp-content/themes/the-wedding-day/vamtam/assets/js/modernizr.min.js )
Rails.application.config.assets.precompile += %w( wp-content/plugins/vamtam-push-menu/js/dist/push-menu.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/plugins/wp-retina-2x/js/retina.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/themes/the-wedding-day/vamtam/assets/js/plugins/thirdparty/jquery.transit.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/themes/the-wedding-day/vamtam/assets/js/plugins/thirdparty/jquery.matchheight.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/themes/the-wedding-day/vamtam/assets/js/plugins/thirdparty/jquery.bxslider.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/jquery/ui/core.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/jquery/ui/effect.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/jquery/ui/widget.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/mediaelement/mediaelement-and-player.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/mediaelement/wp-mediaelement.min.js  )
Rails.application.config.assets.precompile += %w( wp-content/themes/the-wedding-day/vamtam/assets/js/all.min.js  )
Rails.application.config.assets.precompile += %w( wp-includes/js/wp-embed.min.js  )


